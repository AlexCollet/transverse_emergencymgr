package Data;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class DbManager
{
	private Connection _connEmer;
	
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_EMER_URL = "jdbc:postgresql://localhost:5432/emergencyReact";
	static final String USER = "postgres";
	static final String PASS = "admin";
	
	//CTOR
	public DbManager()
	{
		//Ouverture base de donn�es de l'emergency manager
		_connEmer = null;
		try
		{
			Class.forName(JDBC_DRIVER);
			_connEmer = DriverManager.getConnection(DB_EMER_URL, USER, PASS);
		}
		catch(Exception e)
		{
			e.printStackTrace();
	        System.err.println(e.getClass().getName()+": "+e.getMessage());
	        System.exit(0);
		}
		System.out.println("Opened database EmergencyReact successfully");
	}
	
	//Fermeture bases de donn�es
	public void CloseDB()
	{	
		//Fermeture base de donn�es de l'emergency manager
		try
		{
			_connEmer.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
	        System.err.println(e.getClass().getName()+": "+e.getMessage());
	        System.exit(0);
		}
		System.out.println("Closed database EmergencyReact successfully");
	}
	
	//----------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------
	
	public void getTableContent(String db, String table)
	{
		EDatabases database;
		database = EDatabases.valueOf(db);
		
		if(database != null)
		{
			try
			{
				Statement stmt = (database == EDatabases.DB_Emer ? _connEmer: _connEmer).createStatement();
				ResultSet rs = stmt.executeQuery("SELECT * FROM " + table);
				int row = 0;
				int columnCount = rs.getMetaData().getColumnCount();
				while(rs.next())
				{
				    System.out.print("row " + row + ": (");
				    for (int i = 0; i < columnCount; i++)
				    {
				    	if(i<columnCount-1)
				    	{
				    		System.out.print(rs.getString(i+1) + ", ");
				    	}
				    	else
				    		System.out.print(rs.getString(i+1));
					}
				    System.out.println(")");
				    row++;
				}
				rs.close();
				stmt.close();
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void SetDataOnDB(String request, String stateRequest, String db)
	{		
		EDatabases database;
		database = EDatabases.valueOf(db);
		
		if(database != null)
		{
			try
			{
				Statement stmt = (database == EDatabases.DB_Emer ? _connEmer : _connEmer).createStatement();
				if(stateRequest != null)
				{
					stmt.executeUpdate(stateRequest);
				}
				int res = stmt.executeUpdate(request);
				if(res > 0)
				{
					System.out.println("Done request successfully");
				}
				else
				{
					System.out.println("Done request not successfully");
				}
				stmt.close();
			}
			catch (SQLException se)
			{
				se.printStackTrace();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public void SetDataOnDB(String request, String db)
	{
		EDatabases database;
		database = EDatabases.valueOf(db);
		
		if(database != null)
		{
			try
			{
				Statement stmt = (database == EDatabases.DB_Emer ? _connEmer : _connEmer).createStatement();
				int res = stmt.executeUpdate(request);
				if(res > 0)
				{
					System.out.println("Done request successfully");
				}
				else
				{
					System.out.println("Done request not successfully");
				}
				stmt.close();
			}
			catch (SQLException se)
			{
				se.printStackTrace();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	//----------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------

	//SELECT id, intensityCapacity, speed, state FROM fireTruck WHERE idT=idFS
	//SELECT id, x, y, intensity FROM fire WHERE "new id"
	public List<String> GetAndSetTable1(String request, String db)
	{
		EDatabases database;
		database = EDatabases.valueOf(db);
		
		List<String> param = new ArrayList<String>();
		
		if(database != null)
		{
			try
			{
				Statement stmt = (database == EDatabases.DB_Emer ? _connEmer : _connEmer).createStatement();
				
				System.out.println("Start Request 1");
				
				ResultSet rs = stmt.executeQuery(request);
				int columnCount = rs.getMetaData().getColumnCount();
				while(rs.next())
				{
					for (int i = 0; i < columnCount; i++)
					{
					    if(i<columnCount-1)
					    {
					    	param.add(rs.getString(i+1));
					    	System.out.print(rs.getString(i+1) + ", ");
					    }
					    else
					    {
					    	param.add(rs.getString(i+1));
					    	System.out.println(rs.getString(i+1));
					    }
					}
				}
				rs.close();
				stmt.close();
				
				System.out.println("Request 1 done");
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		
		return param;
	}
	
	//SELECT id, intensityCapacity, speed, state FROM fireTruck WHERE idT=idFS
	//SELECT id, x, y FROM fireStation
	public List<List<String>> GetAndSetTable2(String request, String db)
	{
		EDatabases database;
		database = EDatabases.valueOf(db);
		
		List<List<String>> param = new ArrayList<List<String>>();
		List<String> subParam = new ArrayList<String>();
		
		if(database != null)
		{
			try
			{
				Statement stmt = (database == EDatabases.DB_Emer ? _connEmer : _connEmer).createStatement();
				
				System.out.println("Start Request 2");
				
				ResultSet rs = stmt.executeQuery(request);
				int columnCount = rs.getMetaData().getColumnCount();
				while(rs.next())
				{
				    for (int i = 0; i < columnCount; i++)
				    {
				    	if(i<columnCount-1)
				    	{
				    		subParam.add(rs.getString(i+1));
				    		System.out.print(rs.getString(i+1) + ", ");
				    	}
				    	else
				    	{
				    		subParam.add(rs.getString(i+1));
				    		System.out.println(rs.getString(i+1));
				    	}
					}			    
				    param.add(subParam);
				}
				rs.close();
				stmt.close();
				
				System.out.println("Request 2 done");
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		
		return param;
	}

	//SELECT id FROM fire WHERE id = MAX(id)
	//SELECT idFire FROM intervention WHERE idFire = X
	public String GetUniqueData(String request, String db)
	{
		EDatabases database;
		database = EDatabases.valueOf(db);
		
		String data = null;
		
		if(database != null)
		{
			try
			{
				Statement stmt = (database == EDatabases.DB_Emer ? _connEmer : _connEmer).createStatement();
				
				System.out.println("Start Request 0");
				
				ResultSet rs = stmt.executeQuery(request);		
				
				while(rs.next())
				{
				    	data = rs.getString(1);
				    	System.out.println("data = " + rs.getString(1));
				}
				rs.close();
				stmt.close();
				
				System.out.println("Request 0 done");
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}		
		}
		
		return data;
	}
}
