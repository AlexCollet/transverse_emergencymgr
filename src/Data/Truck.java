package Data;

public class Truck implements java.lang.Comparable<Truck>
{	
	private int mId;
	private int mState;
	private int mSpeed;
	private int mIntensityCapacity;
			
	public Truck(int aId, int aState, int aSpeed, int aIntensityCapacity)
	{
		// TODO Auto-generated constructor stub
		mId = aId;
		mState = aState;
		mSpeed = aSpeed;
		mIntensityCapacity = aIntensityCapacity;
	}
	
	public Truck()
	{
		// TODO Auto-generated constructor stub
	}

	//Assignation d'un/des camion(s) � un feu => cr�ation des interventions
	public Boolean MoveTruck(int mFireId, Truck[] mSelectedTruck, DbManager aDbMgr)
	{
		int truckID = 0;
		String request = null;
		
		//Si il n'y a qu'un seul camion de n�cessaire
		if(mSelectedTruck.length == 1)
		{
			try
			{
				truckID = mSelectedTruck[0].getId();
				System.out.println("On enter: mFireId: " + Integer.toString(mFireId) + ", mSelectedTruck.id: " + Integer.toString(truckID) + ", mSelectedTruck.length: " + Integer.toString(mSelectedTruck.length));
				
				//Just for tests
				//request = "UPDATE \"fireTruck\"" + 
				//		"	SET state=" + Integer.toString(1) + 
				//		"	WHERE id="+ Integer.toString(truckID);
				//aDbMgr.SetDataOnDB(request, "DB_Emer");
				//--------------
				
				request = "INSERT INTO intervention (\"idFire\", \"idFireTruck\") "
						+ "VALUES ('" + Integer.toString(mFireId) + "', '" + Integer.toString(truckID) + "')";
				aDbMgr.SetDataOnDB(request, "DB_Emer");
				
				return true;
			}
			catch(Exception e)
			{
				e.printStackTrace();
		        System.err.println(e.getClass().getName()+": "+e.getMessage());
		        System.exit(0);
			}
		}
		else if(mSelectedTruck.length > 1)		//Si il y a n camions de n�cessaire
		{
			try
			{
				for(int i = 0; i<mSelectedTruck.length; i++)
				{
					System.out.println("mSelectedTruck[i].getId: " + Integer.toString(mSelectedTruck[i].getId()));
					
					truckID = mSelectedTruck[i].getId();					
					System.out.println("On enter: mFireId: " + Integer.toString(mFireId) + ", mSelectedTruck.id: " + Integer.toString(truckID) + ", mSelectedTruck.length: " + Integer.toString(mSelectedTruck.length));
					
					//Just for tests
					//request = "UPDATE \"fireTruck\"" + 
					//		" SET state=" + Integer.toString(1) + 
					//		" WHERE id="+ Integer.toString(truckID);
					//aDbMgr.SetDataOnDB(request, "DB_Emer");
					//--------------
					
					request = "INSERT INTO intervention (\"idFire\", \"idFireTruck\") "
							+ "VALUES ('" + Integer.toString(mFireId) + "', '" + Integer.toString(truckID) + "')";
					aDbMgr.SetDataOnDB(request, "DB_Emer");
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
		        System.err.println(e.getClass().getName()+": "+e.getMessage());
		        System.exit(0);
			}
			
			return true;
		}
		else
		{
			System.out.print("Erreur lors du d�placement du ou des camions !");
			return false;
		}
		
		return false;
	}
	
	//GET
	public int getId() {	return mId;	}	
	public int getState()	{	return mState;	}
	public int getSpeed() {	return mSpeed;	}
	public int getIntensityCapacity()	{	return mIntensityCapacity;	}
	
	//SET
	public void setState(int aState) {	mState = aState;	}
	public void setId(int aId) {	mId = aId;	}
	public void setSpeed(int aSpeed) {	mSpeed = aSpeed;	}
	public void setIntensityCapacity(int aIntenseCapacity) { mIntensityCapacity = aIntenseCapacity;	}

	@Override
	public int compareTo(Truck o)
	{
		// TODO Auto-generated method stub
		return 0;
	}
}
