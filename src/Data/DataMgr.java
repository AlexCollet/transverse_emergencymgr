package Data;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.lang.Math;
import Data.Fire;

public class DataMgr implements IData
{
	private ArrayList<Command> m_supportedCommands;
	private DbManager mDbManager = null;
			
	//CTOR
	public DataMgr(DbManager aDbManager)
	{
		mDbManager = aDbManager;
	}
	
	//R�cup�re les informations d'un feu
	@Override
	public Map<Fire, Position> GetFireInfos(int mFireId)
	{
		// TODO Auto-generated method stub
		
		//SELECT x, y, id, intensity FROM fire WHERE "new id"
		//public Fire(int aLatitude, int aLongitude, int aId, int aIntensity)
		
		List<String> fireParam = new ArrayList<String>();
		
		String request = "SELECT \"fireSensor\".latitude, \"fireSensor\".longitude, fire.id, fire.intensity "
				+ "FROM fire INNER JOIN \"fireSensor\" ON fire.\"idSensor\"=\"fireSensor\".id "
				+ "WHERE fire.id=" + Integer.toString(mFireId);
		
		System.out.println("start request GetFiresInfos");
		
		fireParam = mDbManager.GetAndSetTable1(request, "DB_Emer");
		
		String[] array = fireParam.toArray(new String[0]);
		
		Fire mFire = new Fire(Double.parseDouble(array[0]), Double.parseDouble(array[1]), Integer.parseInt(array[2]), Integer.parseInt(array[3]));
		Position mFirePosition = new Position(Double.parseDouble(array[0]), Double.parseDouble(array[1]));
		
		Map<Fire, Position> lhm = new LinkedHashMap<Fire, Position>();
		lhm.put(mFire, mFirePosition);
		
		System.out.println("request GetFiresInfos done");
		
		return lhm;
	}

	//R�cup�re les id des nouveaux feux
	public int[] GetFiresId(int mFirstId, int mLastId)
	{
		//SELECT id FROM fire WHERE id BETWEEN 'a' AND 'b'		
		List<String> firesId = new ArrayList<String>();		
		
		String request = "SELECT id FROM fire WHERE id BETWEEN '" + Integer.toString(mFirstId) + "' AND '" + Integer.toString(mLastId) +"'";
		
		System.out.println("start request GetFiresId");
		
		firesId = mDbManager.GetAndSetTable1(request, "DB_Emer");
		
		String[] array = firesId.toArray(new String[0]);
		
		System.out.println("Longueur du tableau des FiresId: " + Integer.toString(array.length));
		
		int[] mFiresId = new int[array.length];
		
		for(int i = 0; i<array.length; i++)
		{			
			mFiresId[i] = Integer.parseInt(array[i]);
		}
		
		System.out.println("request GetFiresId done");
		return mFiresId;
	}
	
	//R�cup�re les informations des camions d'une caserne
	@Override
	public Truck[] GetTrucksInfos(int mFirestationId)
	{
		// TODO Auto-generated method stub
		
		//SELECT id, state, speed, intensityCapacity FROM fireTruck WHERE idT=idFS
		//Truck(int aId, int aState, int aSpeed, int aIntensityCapacity)
		
		List<List<String>> truckParam = new ArrayList<List<String>>();
		
		String request = "SELECT id, state, speed, \"intensityCapacity\" FROM \"fireTruck\" WHERE \"fireStation\"=" + Integer.toString(mFirestationId);
		truckParam = mDbManager.GetAndSetTable2(request, "DB_Emer");
		
		Truck[] mAllTrucks = new Truck[truckParam.size()];
		
		String[] array = new String[0];
		Iterator<List<String>> iter = truckParam.iterator();
		while(iter.hasNext())
		{
			array = iter.next().toArray(new String[0]);
		}
		
		int l = 0;
		for(int i = 0; i<array.length; i++)
		{
			Truck mTruck = new Truck(Integer.parseInt(array[i]), Integer.parseInt(array[i+1]), Integer.parseInt(array[i+2]), Integer.parseInt(array[i+3]));
			mAllTrucks[l] = mTruck;
			
			l++;
			i+=3;
		}
		
		return mAllTrucks;
	}
	
	//R�cup�re les informations de toutes les casernes
	@Override
	public Map<Firestation[], Position[]> GetFirestationInfos()
	{
		// TODO Auto-generated method stub
				
		//SELECT id, x, y FROM fireStation
		//public Firestation(int aId, int aLatitude, int aLongitude)
		
		List<List<String>> firestationParam = new ArrayList<List<String>>();
		
		String request = "SELECT id, latitude, longitude FROM \"fireStation\"";
		firestationParam = mDbManager.GetAndSetTable2(request, "DB_Emer");
		
		System.out.println("firestationParam.length: " + Integer.toString(firestationParam.size()));	//2

		Firestation[] mAllFirestation = new Firestation[firestationParam.size()];
		Position[] mAllFirestationPosition = new Position[firestationParam.size()];
		
		String[] array = new String[0];
		Iterator<List<String>> iter = firestationParam.iterator();
		while(iter.hasNext())
		{
			System.out.println("iterator: " + iter.next());			
			array = iter.next().toArray(new String[0]);
		}
		
		int l = 0;
		for(int i = 0; i<array.length; i++)
		{
			Firestation mFirestation = new Firestation(Integer.parseInt(array[i]), Double.parseDouble(array[i+1]), Double.parseDouble(array[i+2]));
			Position mFirestationPosition = new Position(Double.parseDouble(array[i+1]), Double.parseDouble(array[i+2]));
			
			mAllFirestation[l] = mFirestation;
			mAllFirestationPosition[l] = mFirestationPosition;
			l++;
			i+=2;
		}		
		
		Map<Firestation[], Position[]> aFS = new LinkedHashMap<Firestation[], Position[]>();
		aFS.put(mAllFirestation, mAllFirestationPosition);
		
		return aFS;
	}

	//V�rifie la disponibilit� d'un camion selon son �tat
	@Override
	public Boolean GetStateInfos(int mTruckId)
	{
		// TODO Auto-generated method stub
		
		List<List<String>> stateInfos = new ArrayList<List<String>>();
		
		String request = "SELECT intervention.\"idFireTruck\", \"fireTruck\".state "
				+ "FROM intervention "
				+ "JOIN \"fireTruck\" ON \"fireTruck\".id=intervention.\"idFireTruck\" "
				+ "WHERE \"fireTruck\".id=" + Integer.toString(mTruckId) ;
		
		stateInfos = mDbManager.GetAndSetTable2(request, "DB_Emer");
		
		Iterator<List<String>> iter = stateInfos.iterator();
		while(iter.hasNext())
		{
			//System.out.println("State iterator: " + iter.next());
			String[] array = iter.next().toArray(new String[0]);
			
			System.out.println("array iterator: " + array[0] + ", " + array[1]);
			
			if(array.length != 0)
			{
				if(array[0] == Integer.toString(mTruckId) && array[1] == Integer.toString(2) || array[1] == Integer.toString(1))
				{
					System.out.println("Camion non disponible");
					return true;
				}
			}
		}

		System.out.println("Camion disponible");
		return false;
	}
	
	//Calcul la distance entre les casernes et le feu
	//et retourne la position de la caserne disponible la plus proche
	@Override
	public Position CalculDistance(Position afirePos, Position[] aFirestationsPosition, int mIter)
	{
		// TODO Auto-generated method stub
		
		double exp = 2;										//exposant -> mise au carr�
		double dis;											//valeur de la distance
		Map<Integer, Double> lhm = new LinkedHashMap<>();	//Association ID Firestation <-> valeur distance
		int lhmId = 0;										//Cl� de la plus petite distance
		
		//Calcul de distance entre l'incendie et chaque caserne
		for(int i = 0; i < aFirestationsPosition.length; i++)
		{
			dis = Math.sqrt(Math.pow(afirePos.getM_x()-aFirestationsPosition[i].getM_x(), exp) +
					Math.pow(afirePos.getM_y()-aFirestationsPosition[i].getM_y(), exp));
			
			lhm.put(i, dis);
		}
		
		//Tri par ordre croissant des distances
		lhm.entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		
		//R�cup�ration du premier �l�ment		
		Integer key;
		Double value = (double) 0;
		
		for(final Entry<Integer, Double> entry : lhm.entrySet())
		{
			key = entry.getKey();
			if(key == mIter-1)
			{
				lhmId = key;
				value = entry.getValue();
			}			
		}
		
		System.out.println("Distance entre la caserne et le feu: " + Double.toString(value));
		
		return aFirestationsPosition[lhmId];
	}
	
	//R�cup�re le/les camions disponibles et n�cessaires � une intervention
	@Override
	public Truck[] SelectTruck(int mFirestationId, Truck[] mAllTrucks, int mFireIntensity)
	{		
		// TODO Auto-generated method stub
		Truck[] truckCompare = new Truck[0];
		Truck[] selectedTruck;
		int nbTrucks = 0;
		
		System.out.println("D�but r�cup�ration Infos camions");
		mAllTrucks = GetTrucksInfos(mFirestationId);		//Select camion		
		System.out.println("mAllTrucks: " + mAllTrucks.length);		
		System.out.println("Fin r�cup�ration Infos camions");
		
		System.out.println("D�but comparaison capacit� camions");
		truckCompare = CompareIntensityCapacity(mAllTrucks, mFireIntensity);
		
		if(truckCompare == null)
		{
			System.out.println("SelectTruck: Pas de camion disponibles");
			return null;
		}
		
		System.out.println("truckCompare.length: " + truckCompare.length + ", contenu:" + truckCompare);		
		System.out.println("Fin comparaison capacit� camions");
		
		if(truckCompare.length == 0)		//Si pas de camion dispo
		{
			System.out.println("SelectTruck: Pas de camion disponibles");
			return null;
		}
		else if(truckCompare.length == 1)		//Si capacit� du camion >= intensit� du feu
		{
			selectedTruck = new Truck[1];
			selectedTruck[0] = truckCompare[0];
			System.out.println("SelectTruck: 1 camions de capacit� >= intensit� du feu dispo");
			return selectedTruck;
		}
		else if(truckCompare.length >= 2)		//Si combinaison de n camions
		{			
			for(int i = 0; i<truckCompare.length; i++)
			{
				if(truckCompare[i] != null)
				{
					nbTrucks++;
				}
			}			
			
			//selectedTruck = new Truck[truckCompare.length];
			selectedTruck = new Truck[nbTrucks];
			for(int i = 0; i<truckCompare.length; i++)
			{
				selectedTruck[i] = truckCompare[i];
			}
			System.out.println("SelectTruck: combinaison de n camions dispo");
			return selectedTruck;
		}
		
		selectedTruck = null;
		return selectedTruck;
	}
	
	//Compare les capacit�s des camions d'une caserne en vue de les affecter � une intervention
	@Override
	public Truck[] CompareIntensityCapacity(Truck[] mAllTrucks, int mFireIntensity)
	{
		// TODO Auto-generated method stub
		
		System.out.println("D�but de la comparaison des camions");
		
		int h = 0; int e = 0; int l = 0; int somme = 0; int c =0;
		
		Truck[] highCapacity = new Truck[mAllTrucks.length];
		Truck[] equalCapacity = new Truck[mAllTrucks.length];
		Truck[] lowCapacity = new Truck[mAllTrucks.length];
		Truck[] pairFinalTruckCapacity;
		
		System.out.println("R�partition des camions");
		
		//R�partition des camions d'une caserne suivant leur capacit� � �teindre un feu
		for(int i = 0; i<mAllTrucks.length; i++)
		{
			int _idTruck = mAllTrucks[i].getId();
			
			System.out.println("Intensit� du feu: " + Integer.toString(mFireIntensity));
			System.out.println("Id du camion: " + Integer.toString(mAllTrucks[i].getId()) + ", capacit�: " + Integer.toString(mAllTrucks[i].getIntensityCapacity()));
			
			if(GetStateInfos(_idTruck) != true)		//"Camion disponible"
			{
				if(mAllTrucks[i].getState() != 1)
				{
					if(mAllTrucks[i].getIntensityCapacity() == mFireIntensity)
					{
						equalCapacity[e] = mAllTrucks[i];
						e++;
					}
					else if(mAllTrucks[i].getIntensityCapacity() > mFireIntensity)
					{
						highCapacity[h] = mAllTrucks[i];
						h++;
					}
					else
					{
						lowCapacity[l] = mAllTrucks[i];
						l++;
					}
				}
			}
		}
		
		System.out.println("V�rification disponibilit�");
		
		//V�rification de disponibilit�
		if(equalCapacity.length != 0 && equalCapacity[0] != null)
		{
			System.out.println("Camion(s) de capacit� �gale � l'intensit� du feu disponible(s)");
			
			pairFinalTruckCapacity = new Truck[1];
			pairFinalTruckCapacity[0] = equalCapacity[0];
			
			System.out.println("Fin de la comparaison des camions");
			System.out.println("pairFinalTruckCapacity1: " + pairFinalTruckCapacity.length);
			return pairFinalTruckCapacity;
		}
		else if(highCapacity.length != 0 && highCapacity[0] != null)
		{
			System.out.println("Camion(s) de capacit� sup�rieure � l'intensit� du feu disponible(s)");
			
			pairFinalTruckCapacity = new Truck[1];
			pairFinalTruckCapacity[0] = highCapacity[0];
			
			System.out.println("Fin de la comparaison des camions");
			System.out.println("pairFinalTruckCapacity2: " + pairFinalTruckCapacity.length);
			return pairFinalTruckCapacity;
		}
		else if(lowCapacity.length > 1 && lowCapacity[0] != null)
		{
			pairFinalTruckCapacity = new Truck[lowCapacity.length];
			//V�rification de combinaisons
			for(int i = 0; i<lowCapacity.length; i++)
			{
				//if(lowCapacity[i])
				somme += lowCapacity[i].getIntensityCapacity();
			}
			
			//Tri des camions selon leur capacit� par ordre d�croissant
			if(somme >= mFireIntensity)
			{
				ArrayList<Truck> list = new ArrayList<Truck>(Arrays.asList(lowCapacity));				
				
				Collections.sort(list, new Comparator<Truck>(){
					@Override 
					public int compare(Truck tc1, Truck tc2)
					{
						return ((Integer)tc1.getIntensityCapacity()).compareTo((Integer)tc2.getIntensityCapacity());
					}
					}.reversed());
				
				while(list.isEmpty() == false && somme > 0)
				{
					pairFinalTruckCapacity[c] = list.get(c);
					somme -= list.get(c).getIntensityCapacity();
					c++;
				}
				
				System.out.println("Ensemble de camions disponibles");
				System.out.println("Fin de la comparaison des camions");
				System.out.println("pairFinalTruckCapacity: " + pairFinalTruckCapacity.length);
				return pairFinalTruckCapacity;
			}
		}
		else
		{
			pairFinalTruckCapacity = null;
			System.out.println("Probl�me, pas de camions disponibles !");
			return pairFinalTruckCapacity;
		}

		return pairFinalTruckCapacity;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	
	@Override
	public void registerCmd(Command cmd)
	{
		// TODO Auto-generated method stub
		m_supportedCommands.add(cmd);
	}

	@Override
	public Command[] getAllCommands()
	{
		// TODO Auto-generated method stub
		Command[] cmds = new Command[m_supportedCommands.size()];
		cmds = m_supportedCommands.toArray(cmds);
		return cmds;
	}
}
