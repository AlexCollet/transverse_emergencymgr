package Data;

public class Position
{
	private double mX_pos;
	private double mY_pos;
	
	public Position(double aX_pos, double aY_pos)
	{
		// TODO Auto-generated constructor stub
		mX_pos = aX_pos;
		mY_pos = aY_pos;
	}

	public double getM_x()	{	return mX_pos;	}
	public double getM_y() {	return mY_pos;	}
	public void setM_x(double m_x) {	this.mX_pos = m_x;	}
	public void setM_y(double m_y) {	this.mY_pos = m_y;	}
	
	
	@Override
	public String toString()
	{
		return "(" + mX_pos + ", " + mY_pos + ")";
	}	
}