package Data;

public abstract class Command
{
	public abstract String getName();
	public abstract String getCallSchem();
	public abstract int getNbArgs();
	public abstract void Execute(String[] args);
}
