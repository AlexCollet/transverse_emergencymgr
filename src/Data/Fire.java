package Data;

public class Fire
{
	private int mId;
	private int mIntensity;
	private Position mPosition;

	public Fire(double aLatitude, double aLongitude, int aId, int aIntensity)
	{
		// TODO Auto-generated constructor stub
		mId = aId;
		mIntensity = aIntensity;
		mPosition = new Position(aLatitude, aLongitude);
	}
	
	//GET
	public int getId()	{	return mId;	}
	public int getIntensity()	{	return mIntensity;	}
	public Position getPosition() {	return mPosition;	}
	
	//SET
	public void setId(int aId) {	mId = aId;	}
	public void setIntensity(int aIntensity) {	mIntensity = aIntensity;	}
	public void setPosition(Position aPosition) {	mPosition = aPosition;	}
}
