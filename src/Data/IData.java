package Data;

import java.util.Map;

public interface IData
{
	Map<Fire, Position> GetFireInfos(int aFireId);
	int[] GetFiresId(int aFirstId, int aLastId);
	Boolean GetStateInfos(int aTruckId);
	Truck[] GetTrucksInfos(int aFirestationId);	
	Map<Firestation[], Position[]> GetFirestationInfos();
	
	Position CalculDistance(Position afirePos, Position[] aFirestationsPosition, int aIter);
	Truck[] SelectTruck(int aFirestationId, Truck[] aAllTrucks, int aFireIntensity);
	Truck[] CompareIntensityCapacity(Truck[] mAllTrucks, int aFireIntensity);
	
	void registerCmd(Command cmd);
	Command[] getAllCommands();
}
