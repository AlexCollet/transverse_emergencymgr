package Data;

public class Firestation
{
	private int mId;	
	private Position mPosition;
	
	public Firestation(int aId, double aLatitude, double aLongitude)
	{
		// TODO Auto-generated constructor stub
		mId = aId;
		mPosition = new Position(aLatitude, aLongitude);
	}
	
	//GET
	public int getId()	{	return mId;	}
	public Position getPosition()	{	return mPosition;	}
	
	//SET
	public void setId(int aId) { mId = aId;	}
	public void setPosition(Position aPosition) { mPosition = aPosition;	}
}