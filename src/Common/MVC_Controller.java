package Common;

import Control.IControl;
import Control.EmergencyMgr;
import Data.DataMgr;
import Data.IData;
import View.IView;
import View.Logger;
import Data.DbManager;

public class MVC_Controller
{
	public static IData data;
	public static IView view;
	public static IControl control;
	
	public static DbManager db;
	
	public MVC_Controller()
	{
		view = new Logger();
		db = new DbManager();
		data = new DataMgr(db);
		control = new EmergencyMgr(data, db, 1);
		
		//db.CloseDB();
	}
}
