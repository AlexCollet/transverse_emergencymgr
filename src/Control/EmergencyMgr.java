package Control;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import Common.MVC_Controller;
import Data.Command;
import Data.DbManager;
import Data.Fire;
import Data.Firestation;
import Data.IData;
import Data.Position;
import Data.Truck;

public class EmergencyMgr implements IControl
{
	IData mDataMgr;
	DbManager mDbMgr;
	int mReferentFireId;
	
	Position mFirePosition;					//Position de l'incendie
	Position mFirestationPosition;			//Position d'une caserne unique
	Position[] mAllFirestationPosition;		//Position de toutes les casernes
	
	Truck mTruck;
	Truck[] mAllTrucks;						//Liste des camions d'une caserne
	Fire mFire;
	Firestation mFirestation;				//Caserne unique
	Firestation[] mAllFirestation;			//Liste des casernes
	
	
	public EmergencyMgr(IData aDataMgr, DbManager aDbMgr, int aReferentFireId)
	{
		// TODO Auto-generated constructor stub
		/*setSupportedCommands();
		CmdReader reader = new CmdReader();
		reader.start();*/
		
		mReferentFireId = aReferentFireId;
		mDataMgr = aDataMgr;
		mDbMgr = aDbMgr;		
				
		Timer checkFire = new Timer();
		TimerTask _task = new TimerTask()
		{
			public void run()
			{		
				int[] newFiresId = null;
		
				int tmpId = 0;
				int _refId = mReferentFireId;
				
				Map<Boolean, Integer> fireIdLhm = new LinkedHashMap<>();
				
				fireIdLhm = CheckForNewFire(_refId);		//Regarde si un nouveau feu est apparu
				
				if(fireIdLhm.entrySet().iterator().next().getKey() == true)
				{					
					tmpId = fireIdLhm.entrySet().iterator().next().getValue();
					newFiresId = mDataMgr.GetFiresId(_refId, tmpId);
					
					//----------------------------------------------------------------
					System.out.print("newFiresId: ");
					for(int i =0; i<newFiresId.length; i++)
					{
						System.out.print(Integer.toString(newFiresId[i]) + ", ");
					}
					System.out.println();
					
					System.out.println("Id reference: " + Integer.toString(_refId) + ", Temporary Id (last found): " + Integer.toString(tmpId));
					//----------------------------------------------------------------

					if(newFiresId.length != 0)
					{
						for(int i = 0; i<newFiresId.length; i++)
						{
							if(CheckIfIsInIntervention(newFiresId[i]) == false)	//V�rifie si un feu est en cours de traitement
							{								
								int _iter = i;
								int[] _DupNewFiresId = newFiresId;
								
								//----------------------------------------------------------------
								System.out.println("_iter: " + Integer.toString(_iter) +", _DupNewFiresId: ");
								for(int l =0; l<newFiresId.length; l++)
								{
									System.out.print(Integer.toString(_DupNewFiresId[l]) + ", ");
								}
								System.out.println();
								//----------------------------------------------------------------
								
								//Cr�ation de n Thread pour n feux
								MyThread t = new MyThread(_iter, _DupNewFiresId)
								{								
									public void run()
									{
										System.out.println("Start Thread " + Integer.toString(_iter));
										try
										{
											System.out.println("_DupNewFiresId[_iter]: " + Integer.toString(_DupNewFiresId[_iter]));
											FireAssignment(_DupNewFiresId[_iter]);	//Assignation d'un/des camion(s) � un feu
										}
										catch(Exception e)
										{
											e.printStackTrace();
											System.out.println("Erreur lors du d�roulement du thread !");
											System.exit(0);
										}
									}
								};
								t.start();
								
								try
								{
									t.join();
								}
								catch (InterruptedException e)
								{
									e.printStackTrace();
								}
							}
							else
							{
								System.out.println("Pas de nouveau feu ou intervention en cours sur le feu.");
							}
						}
					}
				}
			}
		};
		checkFire.scheduleAtFixedRate(_task, 0, 5000);
	}

	//Regarde si un nouveau feu est apparu
	@Override
	public Map<Boolean, Integer> CheckForNewFire(int mRefId)
	{
		// TODO Auto-generated method stub
		String _maxFireId = null;
		String request = null;
		int maxFireId = 0;
		Map<Boolean, Integer> newFirelhm = new LinkedHashMap<>();
		
		//SELECT id FROM fire WHERE id = MAX(id)
		//request = "SELECT id FROM fire WHERE id=MAX(id)";
		request = "SELECT id FROM fire WHERE id=(SELECT MAX(id) FROM fire)";
	
		_maxFireId = mDbMgr.GetUniqueData(request, "DB_Emer");
		
		maxFireId = Integer.parseInt(_maxFireId);
		
		if(maxFireId>mRefId)
		{			
			System.out.println("Requ�te s�lection id max effectu�e, sa valeur est: " + Integer.toString(maxFireId));			
			newFirelhm.put(true, maxFireId);			
			return newFirelhm;
		}
		else
		{
			newFirelhm.put(false, 0);
		}
		
		return newFirelhm;
	}
	
	//V�rifie si un feu est en cours de traitement
	public Boolean CheckIfIsInIntervention(int mFireId)
	{
		String isInInter = null;
		String request = null;
		
		request = "SELECT \"idFire\" FROM intervention WHERE \"idFire\"=" + Integer.toString(mFireId);
		isInInter = mDbMgr.GetUniqueData(request, "DB_Emer");
		
		if(isInInter == null)
		{
			return false;
		}
		
		return true;
	}
	
	//Assignation d'un/des camion(s) � un feu
	@Override
	public void FireAssignment(int mfireId)
	{
		// TODO Auto-generated method stub
		int _iter = 1;
		int _id = 0;
		int _intensity = 0;
		Truck[] selectedTruck = new Truck[0];
		Position _pos = null;
		
		System.out.println("Requ�te s�lection position feu d�but�e");
		
		Map<Fire, Position> fireInfos = new LinkedHashMap<Fire, Position>();	
		
		fireInfos = mDataMgr.GetFireInfos(mfireId);							//R�cup�re les informations du feu
		mFire = fireInfos.entrySet().iterator().next().getKey();			//R�cup�re l'id et l'intensit� du feu
		mFirePosition = fireInfos.entrySet().iterator().next().getValue();	//R�cup�re la position du feu
		
		System.out.println("Requ�te s�lection position feu effectu�e");
		
		//----------------------------------------------------------------
		System.out.println("fireInfos: " + mFire.getId() + ", " + mFire.getIntensity());
		System.out.println("FirePos: " + mFirePosition.getM_x() + ", " + mFirePosition.getM_y());
		//----------------------------------------------------------------
		
		_intensity = mFire.getIntensity();
		System.out.println("_intensity: " + Integer.toString(_intensity));
		
		System.out.println("Requ�te s�lection positions des casernes d�but�e");
		Map<Firestation[], Position[]> getFSInfos = new LinkedHashMap<Firestation[], Position[]>();
		getFSInfos = mDataMgr.GetFirestationInfos();		//R�cup�re les informations de toutes les casernes
		
		mAllFirestation = getFSInfos.entrySet().iterator().next().getKey();
		
		//----------------------------------------------------------------
		System.out.println("mAllFirestation: ");
		for(int i =0; i<mAllFirestation.length; i++)
		{
			System.out.print(mAllFirestation[i].getId() + ", " + mAllFirestation[i].getPosition().getM_x() +", " + mAllFirestation[i].getPosition().getM_y());
			System.out.println();
		}
		System.out.println();
		//----------------------------------------------------------------
		
		mAllFirestationPosition = getFSInfos.entrySet().iterator().next().getValue();	//R�cup�re la postion de toute les casernes
		
		//----------------------------------------------------------------
		System.out.println("mAllFirestationPosition: ");
		for(int i =0; i<mAllFirestationPosition.length; i++)
		{
			System.out.print(mAllFirestationPosition[i].getM_x() + ", " + mAllFirestationPosition[i].getM_y());
			System.out.println();
		}
		System.out.println();
		//----------------------------------------------------------------
		
		System.out.println("Requ�te s�lection positions des casernes effectu�e");
				
		do
		{
			System.out.println("Calcul de distance d�but�e");			
			_pos = mDataMgr.CalculDistance(mFirePosition, mAllFirestationPosition, _iter);		// retourne la position de la caserne la plus proche si disponible			
			System.out.println("Position de la caserne la plus proche: " + Double.toString(_pos.getM_x()) + ", " + Double.toString(_pos.getM_y()));
			System.out.println("Calcul de distance effectu�e");
			
			for(int i = 0; i<mAllFirestationPosition.length; i++)
			{
				try
				{
					if(_pos.getM_x() == mAllFirestationPosition[i].getM_x() && _pos.getM_y() == mAllFirestationPosition[i].getM_y())
					{
						_id = mAllFirestation[i].getId();						
						break;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Erreur, pas de correspondance de position trouv�e !");
			        System.exit(0);
				}
			}			
			
			_iter++;
			mAllTrucks = new Truck[0];
			selectedTruck = mDataMgr.SelectTruck(_id, mAllTrucks, _intensity);	//S�lectionne le/les camions disponible
		}
		while((selectedTruck == null ||selectedTruck.length == 0) && _iter<= mAllFirestation.length);

		//-----------------------------------------------------------------------
		if(selectedTruck != null)
		{
			System.out.println("selectedTruck.length: " + selectedTruck.length);
		}
		//-----------------------------------------------------------------------
		
		if(selectedTruck == null || selectedTruck.length == 0)
		{
			System.out.println("Probl�me, aucune caserne ne poss�de de camions disponibles !");
			System.out.println("");
			System.out.println("");
		}
		else
		{
			_iter = 1;			
			System.out.println("Camion dispo");			
			
			try
			{		
				mTruck = new Truck();
				
				mTruck.MoveTruck(mfireId, selectedTruck, mDbMgr);	//Cr�ation de l'intervention
				System.out.println("Camion(s) boug�(s)");
				System.out.println("");
				System.out.println("");
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("Camion(s) non boug�(s)");
				System.out.println("");
				System.out.println("");
			}
		}
	}

	public void setSupportedCommands()
	{
		MVC_Controller.data.registerCmd(new Command()
		{
			@Override public String getName() { return "helloWorld"; }
			@Override public String getCallSchem() { return "helloWorld"; }
			@Override public int getNbArgs() { return 0; }
			@Override public void Execute(String[] args) { MVC_Controller.view.logMessage("Hello World!"); }
		});
		
		MVC_Controller.data.registerCmd(new Command()
		{
			@Override public String getName() { return "hello"; }
			@Override public String getCallSchem() { return "hello (name)"; }
			@Override public int getNbArgs() { return 1; }
			@Override public void Execute(String[] args) { MVC_Controller.view.logMessage("Hello " + args[0] + "!"); }
		});
		
		MVC_Controller.data.registerCmd(new Command()
		{
			@Override public String getName() { return "help"; }
			@Override public String getCallSchem() { return "help"; }
			@Override public int getNbArgs() { return 0; }
			@Override public void Execute(String[] args) { 
				System.out.println("Command list:");
				for (Command supportedCmd : MVC_Controller.data.getAllCommands()) {
					System.out.println(" - " + supportedCmd.getName());
				}
			}
		});
	}
	
	@Override
	public void executeCmd(String cmd)
	{
		// TODO Auto-generated method stub
		String[] splitted = cmd.split(" ");
		String cmdName = splitted[0];
		String[] args = new String[0];
		if(splitted.length > 1)
		{
			args = Arrays.copyOfRange(splitted, 1, splitted.length);
		}
		
		for (Command supportedCmd : MVC_Controller.data.getAllCommands())
		{
			if(cmdName.compareTo(supportedCmd.getName()) == 0)
			{
				if(args.length == supportedCmd.getNbArgs())
				{
					supportedCmd.Execute(args);
				}
				else
				{
					MVC_Controller.view.logWarning("Incorrect usage of command, usage schem is:\n      " + supportedCmd.getCallSchem());
				}
				return;
			}
		}
		MVC_Controller.view.logWarning("Unknown command, type \"help\" to get the list of command");
	}
}

class MyThread extends Thread
{
    int k;
    int[] tab;
    public MyThread(int i, int[] bat)	
    {
            k = i;
            tab = bat;
    }
}
