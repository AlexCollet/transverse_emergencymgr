package Control;

import java.util.Scanner;
import Common.MVC_Controller;

public class CmdReader extends Thread
{
	public void Run()
	{
		Scanner sc = new Scanner(System.in);
		while(true)
		{
			System.out.print("$");
			
			//Move cursor to end
			char escCode = 0x1B;
			int row = 10;
			int column = 10;
			
			String cmd = sc.nextLine();
			MVC_Controller.control.executeCmd(cmd);
		}
	}
}
