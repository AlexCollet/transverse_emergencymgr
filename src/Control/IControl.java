package Control;

import java.util.Map;

public interface IControl
{
	void FireAssignment(int aFireId);
	Map<Boolean, Integer> CheckForNewFire(int aRefId);
	void executeCmd(String cmd);
}
