package View;

public class Logger implements IView
{
	private EMessageTypes m_logLevel;

	public Logger()
	{
		m_logLevel = EMessageTypes.Info;
	}
	
	@Override
	public void setLogLevel(EMessageTypes level)
	{
		// TODO Auto-generated method stub
		m_logLevel = level;
	}

	@Override
	public void logMessage(String msg, EMessageTypes level)
	{
		// TODO Auto-generated method stub
		if(matchLogLevel(level))
		{
			if(level.ordinal() >= EMessageTypes.Error.ordinal())
			{
				System.err.println(level.toString() + ": \"" + msg + "\"");
			}
		}
	}

	@Override
	public void logMessage(String msg)
	{
		// TODO Auto-generated method stub
		EMessageTypes level = EMessageTypes.Info;
		if(matchLogLevel(level))
		{
			System.out.println(level.toString() + ": \"" + msg + "\"");
		}
	}

	@Override
	public void logError(String msg)
	{
		// TODO Auto-generated method stub
		EMessageTypes level = EMessageTypes.Error;
		if(matchLogLevel(level))
		{
			System.err.println(level.toString() + ": \"" + msg + "\"");
		}
	}

	@Override
	public void logWarning(String msg)
	{
		// TODO Auto-generated method stub
		EMessageTypes level = EMessageTypes.Warning;
		if(matchLogLevel(level))
		{
			System.out.println(level.toString() + ": \"" + msg + "\"");
		}
	}
	
	
	private boolean matchLogLevel(EMessageTypes level)
	{		
		return level.ordinal() >= m_logLevel.ordinal();
	}
}
