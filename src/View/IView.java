package View;

public interface IView
{
	void setLogLevel(EMessageTypes level);
	void logMessage(String msg, EMessageTypes level);
	void logMessage(String msg);
	void logError(String msg);
	void logWarning(String msg);
}
