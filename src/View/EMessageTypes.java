package View;

public enum EMessageTypes
{
	Verbose,
	Info,
	Warning,
	Error,
	Critical
}
